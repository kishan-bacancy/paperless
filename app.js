import bodyParser from "body-parser";
import express from "express";
import cors from "cors";
import cookieParser from "cookie-parser";
import session from "express-session";
import expressLayout from "express-ejs-layouts";
import morgan from "morgan";
import path from "path";
import dotenv from "dotenv";
import flash from "req-flash";

import * as passportMiddleware from "./src/controllers/authController";
import passport from "passport";
import employeeRoutes from "./src/routes/employeeRoutes";
import adminRoutes from "./src/routes/adminRoutes";
require("./src/config/sequelize");
const app = express();

if (process.env.NODE_ENVIRONMENT === "prod") {
  dotenv.config();
} else if (process.env.NODE_ENVIRONMENT === "stag") {
  dotenv.config({ path: ".env.stg" });
} else {
  dotenv.config({ path: ".env" });
}
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan("dev"));
app.use(cookieParser());
app.use(
  session({
    saveUninitialized: true,
    secret: "adjhalfj!@",
    resave: true
  })
);
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

passportMiddleware.local;
passportMiddleware.serializaUser;
passportMiddleware.deserializeUser;

app.set("views", path.join(__dirname, "/src/views"));
app.use(express.static(path.join(__dirname, "/src/public")));
app.set("view engine", "ejs");
app.use(expressLayout);
app.use(cors());

app.use("/", adminRoutes);
app.use("/api/admin", adminRoutes);
app.use(employeeRoutes);

app.use(function(req, res) {
  res.redirect("/api/admin/login");
});

module.exports = app;