$(document).ready(function () {
  $("#updateEmployee, #createEmployee").submit(function (e) {
    $(".error").remove();
    let name = $("#name").val();
    if (name == "") {
      e.preventDefault();
      $("#name").after(
        '<span class="error">This field is required</span>'
      );
    } else if (!/^[a-zA-Z ]+[a-zA-Z]+$/.test(name)) {
      e.preventDefault();
      $("#name").after(
        '<span class="error">Please enter valid input</span>'
      );
    }
    let dob = $("#dob").val();
    if (dob == "") {
      e.preventDefault();
      $("#dob").after(
        '<span class="error">This field is required</span>'
      );
    }
    let email = $("#email").val();
    if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email) || email == "") {
      e.preventDefault();
      $("#email").after(
        '<span class="error">Enter valid Email</span>'
      );
    }
    let aadhar_no = $("#aadhar_no").val();
    if (aadhar_no == "") {
      e.preventDefault();
      $("#Password").after(
        '<span class="error">This field is required</span>'
      );
    } else if (!/^[0-9]{12}$/.test(aadhar_no)) {
      e.preventDefault();
      $("#aadhar_no").after(
        '<span class="error">Please Enter Valid Aadhar Number and It must be of 12 numbers only</span>'
      );
    }
    let pancard_no = $("#pan_no").val();
    if (pancard_no == "") {
      e.preventDefault();
      $("#pan_no").after(
        '<span class="error">This field is required</span>'
      );
    } else if (!/^[A-Za-z0-9]{10}$/.test(pancard_no)) {
      e.preventDefault();
      $("#pan_no").after(
        '<span class="error">Please Enter Valid Pan card Number and It must be of 10 characters only</span>'
      );
    }
    let epic_no = $("#epicno").val();
    if (epic_no == "") {
      e.preventDefault();
      $("#epicno").after(
        '<span class="error">This field is required</span>'
      );
    } else if (!/^[A-Za-z0-9]{10}$/.test(epic_no)) {
      e.preventDefault();
      $("#epicno").after(
        '<span class="error">Please Enter Valid EPIC Number and It must be of 10 charcaters only</span>'
      );
    }
    let mobile_no = $("#mobile_no").val();
    if (mobile_no == "") {
      e.preventDefault();
      $("#mobile_no").after(
        '<span class="error">This field is required</span>'
      );
    } else if (!/^[6-9]+[0-9]{9}/.test(mobile_no)) {
      e.preventDefault();
      $("#mobile_no").after(
        '<span class="error">Enter valid mobile number</span>'
      );
    } else if (mobile_no.length != 10) {
      e.preventDefault();
      $("#mobile_no").after(
        '<span class="error">mobile must be 10 digit only.</span>'
      );
    }
    let address = $("#address").val();
    if (address == "") {
      e.preventDefault();
      $("#address").after(
        '<span class="error">This field is required</span>'
      );
    }
    let university_name = $("#university_name").val();
    if (university_name == "") {
      e.preventDefault();
      $("#university_name").after(
        '<span class="error">This field is required</span>'
      );
    }
    let college_name = $("#college_name").val();
    if (college_name == "") {
      e.preventDefault();
      $("#college_name").after(
        '<span class="error">This field is required</span>'
      );
    }
    let enrollment_no = $("#enrollment_no").val();
    if (enrollment_no == "") {
      e.preventDefault();
      $("#enrollment_no").after(
        '<span class="error">This field is required</span>'
      );
    }
    let college_place = $("#college_place").val();
    if (college_place == "") {
      e.preventDefault();
      $("#college_place").after(
        '<span class="error">This field is required</span>'
      );
    }
    let grade = $("#grade").val();
    if (grade == "") {
      e.preventDefault();
      $("#grade").after(
        '<span class="error">This field is required</span>'
      );
    }
    let category = $('#category').val();
    if (category == 2) {
      let salary = $("#salary").val();
      if (salary == "") {
        e.preventDefault();
        $("#salary").after(
          '<span class="error">This field is required</span>'
        );
      }
      let company_name = $("#company_name").val();
      if (company_name == "") {
        e.preventDefault();
        $("#company_name").after(
          '<span class="error">This field is required</span>'
        );
      }
      let joining_date = $("#joining_date").val();
      if (joining_date == "") {
        e.preventDefault();
        $("#joining_date").after(
          '<span class="error">This field is required</span>'
        );
      }
      let resign_date = $("#resign_date").val();
      if (resign_date == "") {
        e.preventDefault();
        $("#resign_date").after(
          '<span class="error">This field is required</span>'
        );
      }
      let company_email = $("#company_email").val();
      if (company_email == "") {
        e.preventDefault();
        $("#company_email").after(
          '<span class="error">This field is required</span>'
        );
      }
      let designation = $("#designation").val();
      if (designation == "") {
        e.preventDefault();
        $("#designation").after(
          '<span class="error">This field is required</span>'
        );
      }
    }
  })

  $("#searchEmp").keyup(function (e) {
    let value = $("#searchEmp").val();
    if (value !== '' || e.keyCode == 8) {
      $.ajax({
        type: 'get',
        url: `/employees/search/${value}`,
        dataType: 'json',
        data: {},
        success: function (data) {
          $("#tablebody").html("");
          for (let i = 0; i < data.emp.length; i++) {
            $("#tablebody").append(`
              <tr>
                <td>${data.emp[i].voterId.name}</td>
                <td>${data.emp[i].email}</td>
                <td>${data.emp[i].voterId.gender}</td>
                <td>${data.emp[i].voterId.address}</td>
                <td>
                  <a href="/employee/${data.emp[i].id}">
                    <span class="fa fa-eye fa-lg eye"></span>
                  </a>&nbsp;
                  <a onclick="return deleteConfirm();" href="/employees/${data.emp[i].id}">
                    <span class="fa fa-trash fa-lg trash"></span>
                  </a>
                </td>
              </tr>
            `);
          }
        }
      })
    }
  })
});

function deleteConfirm() {
  if (!window.confirm("Are you sure want to delete this Record?")) {
    return false;
  } else {
    return true;
  }
}

function enableEdit() {
  let x = document.getElementsByClassName('form-control');
  for (let i = 0; i < x.length; i++) {
    x[i].disabled = false;
  }
  document.getElementById('submit').disabled = false;
  return false;
}

function setStatus() {
  let status = $('#hiddenStatus').val();
  console.log(status, "status")
  let id = $('#hiddenId').val();
  if (status == 'true') {
    status = false;
    $('#idToggle').removeClass('fa fa-toggle-off fa-2x actionOn');
    $('#idToggle').addClass('fa fa-toggle-on fa-2x actionOff');
  }
  else {
    status = true;
    $('#idToggle').removeClass('fa fa-toggle-on fa-2x actionOff');
    $('#idToggle').addClass('fa fa-toggle-off fa-2x actionOn');
  }
  $.ajax({
    type: 'post',
    url: `/employee/status/${id}`,
    dataType: 'json',
    data: { Active: status },
    success: function (data) { }
  })
}