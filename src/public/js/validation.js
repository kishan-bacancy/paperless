$(document).ready(function() {
  $("#aadhar").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      document.getElementById("spanaadhar").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#aadhar").val("");
      return false;
    } else {
      document.getElementById("spanaadhar").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i];
        var fileReader = new FileReader();
        fileReader.onload = function(e) {
          var file = e.target;
          $(
            '<span class="pip">' +
              '<img class="imageThumb" src="' +
              e.target.result +
              '" title="' +
              file.name +
              '"/>' +
              '<br/><span class="remove">Remove image</span>' +
              "</span>"
          ).insertAfter("#aadhar");
          $(".remove").click(function() {
            $(this)
              .parent(".pip")
              .remove();
            $("#" + i).val("");
          });
        };
        fileReader.readAsDataURL(f);
      }
    }
  });

  $("#experience").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("spanexperience").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#experience").val("");
      return false;
    } else {
      document.getElementById("spanexperience").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
        for (var i = 0; i < filesLength; i++) {
          var f = files[i];
          var fileReader = new FileReader();
          fileReader.onload = function(e) {
            var file = e.target;
            $(
              '<span class="pip">' +
                '<img class="imageThumb" src="' +
                e.target.result +
                '" title="' +
                file.name +
                '"/>' +
                '<br/><span class="remove">Remove image</span>' +
                "</span>"
            ).insertAfter("#experience");
            $(".remove").click(function() {
              $(this)
                .parent(".pip")
                .remove();
              $("#" + i).val("");
            });
          };
          fileReader.readAsDataURL(f);
        }
      }
  });

  $("#degree").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("spandegree").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#degree").val("");
      return false;
    } else {
      document.getElementById("spandegree").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
        for (var i = 0; i < filesLength; i++) {
          var f = files[i];
          var fileReader = new FileReader();
          fileReader.onload = function(e) {
            var file = e.target;
            $(
              '<span class="pip">' +
                '<img class="imageThumb" src="' +
                e.target.result +
                '" title="' +
                file.name +
                '"/>' +
                '<br/><span class="remove">Remove image</span>' +
                "</span>"
            ).insertAfter("#degree");
            $(".remove").click(function() {
              $(this)
                .parent(".pip")
                .remove();
              $("#" + i).val("");
            });
          };
          fileReader.readAsDataURL(f);
        }
      }
  });

  $("#salaryslip").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("spansalaryslip").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#salaryslip").val("");
      return false;
    } else {
      document.getElementById("spansalaryslip").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
      if (filesLength != 3) {
        document.getElementById("spansalaryslip").innerHTML =
          "<span style='color: red'>Please upload 3 files</span>";
        // alert("please choose 3 files");
        $("#salaryslip").val("");
        return false;
      } else {
        for (var i = 0; i < filesLength; i++) {
          var f = files[i];
          var fileReader = new FileReader();
          fileReader.onload = function(e) {
            var file = e.target;
            $(
              '<span class="pip">' +
                '<img class="imageThumb" src="' +
                e.target.result +
                '" title="' +
                file.name +
                '"/>' +
                '<br/><span class="remove">Remove image</span>' +
                "</span>"
            ).insertAfter("#salaryslip");
            $(".remove").click(function() {
              $(this)
                .parent(".pip")
                .remove();
              $("#" + i).val("");
            });
          };
          fileReader.readAsDataURL(f);
        }
      }
    }
  });

  $("#passport").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("spanpassport").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#passport").val("");
      return false;
    } else {
      document.getElementById("spanpassport").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
      if (filesLength != 2) {
        document.getElementById("spanpassport").innerHTML =
          "<span style='color: red'>Please upload 2 files</span>";
        $("#passport").val("");
        return false;
      } else {
        for (var i = 0; i < filesLength; i++) {
          var f = files[i];
          var fileReader = new FileReader();
          fileReader.onload = function(e) {
            var file = e.target;
            $(
              '<span class="pip">' +
                '<img class="imageThumb" src="' +
                e.target.result +
                '" title="' +
                file.name +
                '"/>' +
                '<br/><span class="remove">Remove image</span>' +
                "</span>"
            ).insertAfter("#passport");
            $(".remove").click(function() {
              $(this)
                .parent(".pip")
                .remove();
              $("#" + i).val("");
            });
          };
          fileReader.readAsDataURL(f);
        }
      }
    }
  });

  $("#freshersvoterid").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("spanfreshersvoterid").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#freshersvoterid").val("");
      return false;
    } else {
      document.getElementById("spanfreshersvoterid").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
      if (filesLength != 2) {
        document.getElementById("spanfreshersvoterid").innerHTML =
          "<span style='color: red'>Please upload 2 files</span>";
        $("#freshersvoterid").val("");
        return false;
      } else {
        for (var i = 0; i < filesLength; i++) {
          var f = files[i];
          var fileReader = new FileReader();
          fileReader.onload = function(e) {
            var file = e.target;
            $(
              '<span class="pip">' +
                '<img class="imageThumb" src="' +
                e.target.result +
                '" title="' +
                file.name +
                '"/>' +
                '<br/><span class="remove">Remove image</span>' +
                "</span>"
            ).insertAfter("#freshersvoterid");
            $(".remove").click(function() {
              $(this)
                .parent(".pip")
                .remove();
              $("#" + i).val("");
            });
          };
          fileReader.readAsDataURL(f);
        }
      }
    }
  });

  $("#expvoterid").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("expspanvoterid").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#expvoterid").val("");
      return false;
    } else {
      document.getElementById("expspanvoterid").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
      if (filesLength != 2) {
        document.getElementById("expspanvoterid").innerHTML =
          "<span style='color: red'>Please upload 2 files</span>";
        // alert("please choose 3 files");
        $("#expvoterid").val("");
        return false;
      } else {
        for (var i = 0; i < filesLength; i++) {
          var f = files[i];
          var fileReader = new FileReader();
          fileReader.onload = function(e) {
            var file = e.target;
            $(
              '<span class="pip">' +
                '<img class="imageThumb" src="' +
                e.target.result +
                '" title="' +
                file.name +
                '"/>' +
                '<br/><span class="remove">Remove image</span>' +
                "</span>"
            ).insertAfter("#expvoterid");
            $(".remove").click(function() {
              $(this)
                .parent(".pip")
                .remove();
              $("#" + i).val("");
            });
          };
          fileReader.readAsDataURL(f);
        }
      }
    }
  });

  $("#pancard").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("spanpancard").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#pancard").val("");
      return false;
    } else {
      document.getElementById("spanpancard").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i];
        var fileReader = new FileReader();
        fileReader.onload = function(e) {
          var file = e.target;
          $(
            '<span class="pip">' +
              '<img class="imageThumb" src="' +
              e.target.result +
              '" title="' +
              file.name +
              '"/>' +
              '<br/><span class="remove">Remove image</span>' +
              "</span>"
          ).insertAfter("#pancard");
          $(".remove").click(function() {
            $(this)
              .parent(".pip")
              .remove();
            $("#" + i).val("");
          });
        };
        fileReader.readAsDataURL(f);
      }
    }
  });

  $("#exppancard").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("expspanpancard").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#exppancard").val("");
      return false;
    } else {
      document.getElementById("expspanpancard").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i];
        var fileReader = new FileReader();
        fileReader.onload = function(e) {
          var file = e.target;
          $(
            '<span class="pip">' +
              '<img class="imageThumb" src="' +
              e.target.result +
              '" title="' +
              file.name +
              '"/>' +
              '<br/><span class="remove">Remove image</span>' +
              "</span>"
          ).insertAfter("#exppancard");
          $(".remove").click(function() {
            $(this)
              .parent(".pip")
              .remove();
            $("#" + i).val("");
          });
        };
        fileReader.readAsDataURL(f);
      }
    }
  });

  $("#photo").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("spanphoto").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#photo").val("");
      return false;
    } else {
      document.getElementById("spanphoto").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i];
        var fileReader = new FileReader();
        fileReader.onload = function(e) {
          var file = e.target;
          $(
            '<span class="pip">' +
              '<img class="imageThumb" src="' +
              e.target.result +
              '" title="' +
              file.name +
              '"/>' +
              '<br/><span class="remove">Remove image</span>' +
              "</span>"
          ).insertAfter("#photo");
          $(".remove").click(function() {
            $(this)
              .parent(".pip")
              .remove();
            $("#" + i).val("");
          });
        };
        fileReader.readAsDataURL(f);
      }
    }
  });

  $("#voterid").change(function(e) {
    const size = (this.files[0].size / 1024 / 1024).toFixed(2);
    if (size > 4) {
      // alert("File must be less than 4 MB");
      document.getElementById("spanvoterid").innerHTML =
        "<span style='color: red'>File must be less than 4 MB</span>";
      $("#voterid").val("");
      return false;
    } else {
      document.getElementById("spanvoterid").innerHTML = "";
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i];
        var fileReader = new FileReader();
        fileReader.onload = function(e) {
          var file = e.target;
          $(
            '<span class="pip">' +
              '<img class="imageThumb" src="' +
              e.target.result +
              '" title="' +
              file.name +
              '"/>' +
              '<br/><span class="remove">Remove image</span>' +
              "</span>"
          ).insertAfter("#voterid");
          $(".remove").click(function() {
            $(this)
              .parent(".pip")
              .remove();
            $("#" + i).val("");
          });
        };
        fileReader.readAsDataURL(f);
      }
    }
  });

  $("#login").submit(function(e) {
    let Email = $("#Email").val();
    let Password = $("#Password").val();
    let error = $(".error").remove();

    if (
      !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(Email) ||
      Email == ""
    ) {
      e.preventDefault();
      $("#login-email").after(
        '<div style="margin-top:-2.5%; color:red;" class="error">Enter valid Email</div>'
      );
    }
    if (Password == "") {
      e.preventDefault();
      $("#login-password").after(
        '<div style="margin-top:-2.5%; color:red;" class="error">Enter valid Password</div>'
      );
    }
  });

  $("#logout").click(function(e) {
    $.ajax({
      url: "/api/admin/logout",
      method: "POST",
      success: function(res) {
        window.location = "login";
      }
    });
  });

  $("#sidebarlogout").click(function(e) {
    $.ajax({
      url: "/api/admin/logout",
      method: "POST",
      success: function(res) {
        window.location = "login";
      }
    });
  });

  $("#createAdmin").submit(function(e) {
    let FirstName = $("#FirstName").val();
    let LastName = $("#LastName").val();
    let Email = $("#Email").val();
    let Password = $("#Password").val();
    let CPassword = $("#CPassword").val();
    let Mobile = $("#Mobile").val();

    $(".error").remove();

    if (FirstName == "") {
      e.preventDefault();
      $("#FirstName").after(
        '<span class="error">This field is required</span>'
      );
    } else if (!/^[a-zA-Z]+$/.test(FirstName)) {
      e.preventDefault();
      $("#FirstName").after(
        '<span class="error">Please enter valid input</span>'
      );
    }
    if (FirstName.length > 15) {
      e.preventDefault();
      $("#FirstName").after(
        '<span class="error">First Name should maximum 15 characters only.</span>'
      );
    }
    if (LastName == "") {
      e.preventDefault();
      $("#LastName").after('<span class="error">This field is required</span>');
    } else if (!/^[a-zA-Z]+$/.test(LastName)) {
      e.preventDefault();
      $("#LastName").after(
        '<span class="error">Please enter valid input</span>'
      );
    } else if (LastName.length > 15) {
      e.preventDefault();
      $("#LastName").after(
        '<span class="error">Last Name should maximum 15 characters only.</span>'
      );
    }
    if (
      !/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(Email) ||
      Email == ""
    ) {
      e.preventDefault();
      $("#Email").after('<span class="error">Enter valid Email</span>');
    }
    if (Password == "") {
      e.preventDefault();
      $("#Password").after('<span class="error">This field is required</span>');
    } else if (Password.length < 8 || Password.length > 32) {
      e.preventDefault();
      $("#Password").after(
        '<span class="error">Password must be between 8 to 32 character long.</span>'
      );
    }
    if (CPassword == "") {
      e.preventDefault();
      $("#CPassword").after(
        '<span class="error">This field is required</span>'
      );
    } else if (CPassword.length < 8 || CPassword.length > 32) {
      e.preventDefault();
      $("#CPassword").after(
        '<span class="error">Confirm Password must be between 8 to 32 character long.</span>'
      );
    } else if (Password != CPassword) {
      e.preventDefault();
      $("#CPassword").after(
        '<span class="error">Password and Confirm password must be same.</span>'
      );
    }
    if (Mobile == "") {
      e.preventDefault();
      $("#Mobile").after('<span class="error">This field is required</span>');
    } else if (!/^[6-9]+[0-9]{9}/.test(Mobile)) {
      e.preventDefault();
      $("#Mobile").after(
        '<span class="error">Enter Valid Mobile Number</span>'
      );
    } else if (Mobile.length != 10) {
      e.preventDefault();
      $("#Mobile").after(
        '<span class="error">Mobile must be 10 digit only.</span>'
      );
    }
  });
});
function checkConfirm() {
  if (!window.confirm("Are you sure want to delete this Record?")) {
    return false;
  } else {
    return true;
  }
}
