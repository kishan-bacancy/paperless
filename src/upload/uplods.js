const multer = require("multer");
const path = require("path");
const fs = require("fs");

const storage = multer.diskStorage({
  destination: async function(req, file, callback) {
    await fs.mkdirSync(
      "./src/public/uploads/" + file.fieldname,
      { recursive: true },
      err => {
        if (err) throw err;
      }
    );
    if (file.fieldname === "Salary Slip") {
      callback(null, "./src/public/uploads/" + file.fieldname);
    } else if (file.fieldname === "Experience") {
      callback(null, "./src/public/uploads/" + file.fieldname);
    } else if (file.fieldname === "Aadhar") {
      callback(null, "./src/public/uploads/" + file.fieldname);
    } else if (file.fieldname === "Photo") {
      callback(null, "./src/public/uploads/" + file.fieldname);
    } else if (file.fieldname === "Voter Id") {
      callback(null, "./src/public/uploads/" + file.fieldname);
    } else if (file.fieldname === "Degree") {
      callback(null, "./src/public/uploads/" + file.fieldname);
    } else if (file.fieldname === "PAN Card") {
      callback(null, "./src/public/uploads/" + file.fieldname);
    } else if (file.fieldname === "Passport") {
      callback(null, "./src/public/uploads/" + file.fieldname);
    } else {
      callback(null, "./src/public/uploads/");
    }
  },
  filename: function(req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname) );
  }
});

const upload = multer({
  storage: storage,
  fileFilter: (req, file, callback) => {
    // Allow images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG)$/)) {
      return callback(new Error("Only jpg, jpeg image are allowed."), false);
    }
    callback(null, true);
  }
});

module.exports = { upload };