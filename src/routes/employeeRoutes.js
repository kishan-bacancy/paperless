import express from "express";
import validate from "express-validation";

import { isLoggedIn } from "./../middleware/passportMiddleware";
import { displayEmployees, removeEmployee, getEmployee, updateEmployee,
  addEmployee, updateEmployeeStatus, searchEmployee }
  from "../controllers/employeeController";
import { editEmployeeValidate } from "../controllers/employeeValidator";
import { extraction } from "../controllers/employeeController";
import { upload } from "../../src/upload/uplods";
import { freshersForm, experienceForm } from "../controllers/employeeController";

const router = express.Router();

router.get("/freshersform", isLoggedIn, freshersForm);
router.get("/experienceform", isLoggedIn, experienceForm);
router.post(
  "/preview",isLoggedIn,
  upload.fields([
    { name: "Aadhar", maxCount: 1 },
    { name: "PAN Card", maxCount: 1 },
    { name: "Passport", maxCount: 2 },
    { name: "Voter Id", maxCount: 2 },
    { name: "Degree", maxCount: 1 },
    { name: "Photo", maxCount: 1 },
    { name: "Salary Slip", maxCount: 3 },
    { name: "Experience", maxCount: 1 }
  ]), extraction);
router.post('/employee/create', isLoggedIn, addEmployee);

router.get("/employees", isLoggedIn, displayEmployees);

router.get("/employees/:id", isLoggedIn, removeEmployee);

router.get("/employees/search/:value", isLoggedIn, searchEmployee);

router.get("/employee/:id", isLoggedIn, getEmployee);

router.post(
  "/employee/status/:id",
  isLoggedIn,
  updateEmployeeStatus
);

router.post(
  "/employee/:id",
  isLoggedIn,
  validate(editEmployeeValidate),
  updateEmployee
);

module.exports = router;