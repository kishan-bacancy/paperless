import express from "express";
import passport from "passport";

import validate from "express-validation";
import { isLoggedIn } from "./../middleware/passportMiddleware";
import {
  admin,
  addAdmin,
  getAdmins,
  changeStatus,
  removeAdmin,
  dashboard,
  login,
  logout,
  getAddEmpCategory,
  getAddDocumentCategory
} from "../controllers/userController";
import { registerAdminValidate } from "../controllers/userValidator";

const router = express.Router();

router.get("/", login);
router.get("/login", login);
router.get("/dashboard", isLoggedIn, dashboard);
router.post(
  "/login",
  passport.authenticate("local", {
    successRedirect: "/api/admin/dashboard",
    failureRedirect: "/api/admin/login"
  })
);
router.post("/logout", logout);
router.get("/subadmin", isLoggedIn, admin);
router.post(
  "/1/create/subadmin",
  isLoggedIn,
  validate(registerAdminValidate),
  addAdmin
);
router.get("/admins", isLoggedIn, getAdmins);
router.get("/subadmin/:id", isLoggedIn, removeAdmin);
router.get("/subadmin/:id/status/:status", isLoggedIn, changeStatus);
router.get("/category", isLoggedIn, getAddEmpCategory);
router.get("/document", isLoggedIn, getAddDocumentCategory);

module.exports = router;