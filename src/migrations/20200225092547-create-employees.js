'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      createdBy: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Admins',
          key: 'id',
          as: 'created'
        },
        onDelete: "CASCADE",
      },
      Employee_category: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Employee_categories',
          key: 'id'
        },
        onDelete: "CASCADE",
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false
      },
      modifiedBy: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Admins',
          key: 'id',
          as: 'modified'
        },
        onDelete: "CASCADE",
      },
      photo: {
        type: Sequelize.STRING,
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false
      },
      mobile_no: {
        type: Sequelize.STRING,
        allowNull: false
      },
      voterId: {
        epicno: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        gender: {
          type: Sequelize.STRING,
          allowNull: false
        },
        dob: {
          type: Sequelize.DATE,
          allowNull: false
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false
        },
        address: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        doc_path: {
          front: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          back: {
            type: Sequelize.STRING,
            allowNull: false,
          }
        },
        type: Sequelize.JSON,
        allowNull: false
      },
      aadhar: {
        aadhar_no: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        doc_path: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        type: Sequelize.JSON,
        allowNull: false,
      },
      pancard: {
        pan_no: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        doc_path: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        type: Sequelize.JSON,
        allowNull: false,
      },
      passport: {
        passport_no: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        passport_fileno: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        issue_date: {
          type: Sequelize.DATE,
          allowNull: true,
        },
        expire_date: {
          type: Sequelize.DATE,
          allowNull: true,
        },
        doc_path: {
          front: {
            type: Sequelize.STRING,
            allowNull: true,
          },
          back: {
            type: Sequelize.STRING,
            allowNull: true,
          },
          type: Sequelize.JSON,
          allowNull: true
        },
        type: Sequelize.JSON,
        allowNull: true
      },
      degree: {
        enrollment_no: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        college_name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        university_name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        college_place: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        grade: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        doc_path: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        type: Sequelize.JSON,
        allowNull: true
      },
      salarySlip: {
        salary: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        doc_path: {
          slip1: {
            type: Sequelize.STRING,
            allowNull: false
          },
          slip2: {
            type: Sequelize.STRING,
            allowNull: false
          },
          slip3: {
            type: Sequelize.STRING,
            allowNull: false
          }
        },
        type: Sequelize.JSON,
        allowNull: true
      },
      experience: {
        company_name: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        joining_date: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        resign_date: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        designation: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        company_email: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        doc_path: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        type: Sequelize.JSON,
        allowNull: true
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Employees');
  }
};