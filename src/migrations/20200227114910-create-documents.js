'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Documents', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Employee_category: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Employee_categories',
          key:'id'
        },
        onDelete:'CASCADE',
      },
      Document_category_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Document_categories',
          key:'id'
        },
        onDelete:'CASCADE',
      },
      Document_path: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      Employee_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Employees',
          key:'id'
        },
        onDelete:'CASCADE',
        allowNull: false,
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.dropTable('Documents');
  }
};