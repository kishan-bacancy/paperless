const bcrypt = require("bcrypt");

module.exports = (sequelize, DataTypes) => {
  let Admins = sequelize.define(
    "Admins",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      firstName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false
      },
      role: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 2
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      isActive: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
      },
      contact: {
        type: DataTypes.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: true,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: true,
        type: DataTypes.DATE
      },
      deletedAt: {
        allowNull: true,
        type: DataTypes.DATE
      }
    },
    {
      freezeTableName: true,
      instanceMethods: {
        generateHash(password) {
            return bcrypt.hash(password, bcrypt.genSaltSync(8));
        },
        validPassword(password) {
            return bcrypt.compare(password, this.password);
        }
    }
    } 
  );
  Admins.associate = function(models) {
    Admins.hasMany(models.Employee_categories, {
      foreignKey: "id",
      as: "emp_cate"
    });
  };
  Admins.associate = function(models) {
    Admins.hasMany(models.Document_catagories, {
      foreignKey: "id",
      as: "doc_cate"
    });
  };
  Admins.associate = function(models) {
    Admins.hasMany(models.Employees, {
      foreignKey: 'id'
    });
  };
  Admins.associate = function (models) {
    Admins.hasMany(models.Employees, {
      foreignKey: 'id'
    });
  };
  return Admins;
};