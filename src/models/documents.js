'use strict';
module.exports = (sequelize, DataTypes) => {
  const Documents = sequelize.define('Documents', {
    Employee_category: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Document_category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Document_path: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Employee_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    createdAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, { paranoid: true });

  Documents.associate = function (models) {
    Documents.belongsTo(models.Employee_categories, {
      foreignKey: 'Employee_category',
      targetKey: 'id',
    });
  };
  Documents.associate = function (models) {
    Documents.belongsTo(models.Document_categories, {
      foreignKey: 'Document_category_id',
      targetKey: 'id',
    });
  };
  Documents.associate = function (models) {
    Documents.belongsTo(models.Employees, {
      foreignKey: 'Employee_id',
      targetKey: 'id',
    });
  };
  return Documents;
};