'use strict';
module.exports = (sequelize, DataTypes) => {
  const Document_categories = sequelize.define('Document_categories', {   
    employeeCategory: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    isActive: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdBy: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    createdAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {});
  Document_categories.associate = function (models) {
    Document_categories.belongsTo(models.Employee_categories, {
      foreignKey: 'employeeCategory',
      targetKey: 'id',
    });
  };
  Document_categories.associate = function (models) {
    Document_categories.belongsTo(models.Admins, {
      foreignKey: 'createdBy',
      targetKey: 'id',
    });
  };
  Document_categories.associate = function (models) {
    Document_categories.hasMany(models.Documents, {
      foreignKey: 'id'
    });
  };
  return Document_categories;
};