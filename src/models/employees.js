'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employees = sequelize.define('Employees', {
    createdBy: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    Employee_category: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    modifiedBy: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    photo: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    mobile_no: {
      type: DataTypes.STRING,
      allowNull: false
    },
    voterId: {
      epicno: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      gender: {
        type: DataTypes.STRING,
        allowNull: false
      },
      dob: {
        type: DataTypes.DATEONLY,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      address: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      doc_path: {
        front: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        back: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        type: DataTypes.JSON,
        allowNull: true
      },
      type: DataTypes.JSON,
      allowNull: false
    },
    aadhar: {
      aadhar_no: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      doc_path: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      type: DataTypes.JSON,
      allowNull: false
    },
    pancard: {
      pan_no: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      doc_path: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      type: DataTypes.JSON,
      allowNull: false
    },
    passport: {
      passport_no: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      passport_fileno: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      issue_date: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      expire_date: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      doc_path: {
        front: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        back: {
          type: DataTypes.STRING,
          allowNull: true,
        },
        type: DataTypes.JSON,
        allowNull: true
      },
      type: DataTypes.JSON,
      allowNull: true
    },
    degree: {
      enrollment_no: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      college_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      university_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      college_place: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      grade: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      doc_path: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      type: DataTypes.JSON,
      allowNull: true
    },
    salarySlip: {
      salary: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      doc_path: {
        slip1: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        slip2: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        slip3: {
          type: DataTypes.STRING,
          allowNull: false,
        }
      },
      type: DataTypes.JSON,
      allowNull: true
    },
    experience: {
      company_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      joining_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      resign_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      designation: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      company_email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      doc_path: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      type: DataTypes.JSON,
      allowNull: true
    }
  }, {});
  Employees.associate = function (models) {
    Employees.hasMany(models.Employees, {
      foreignKey: 'id'
    });
  };
  Employees.associate = function (models) {
    Employees.belongsTo(models.Employee_categories, {
      foreignKey: 'Employee_category',
      targetKey: 'id'
    });
  };
  Employees.associate = function (models) {
    Employees.belongsTo(models.Admins, {
      foreignKey: 'createdBy',
      targetKey: 'id',
      as: 'created'
    });
  };
  Employees.associate = function (models) {
    Employees.belongsTo(models.Admins, {
      foreignKey: 'modifiedBy',
      targetKey: 'id',
      as: 'modified'
    });
  };
  return Employees;
};