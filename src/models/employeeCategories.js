'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employee_categories = sequelize.define('Employee_categories', {
    createdBy: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: true,
      type: DataTypes.DATE
    },
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {});
  Employee_categories.associate = function (models) {
    Employee_categories.hasMany(models.Employees, {
      foreignKey: 'id'
    });
  };
  Employee_categories.associate = function (models) {
    Employee_categories.belongsTo(models.Admins, {
      foreignKey: 'createdBy', targetKey: 'id'
    });
  };
  return Employee_categories;
};