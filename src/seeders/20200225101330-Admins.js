"use strict";

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "Admins",
      [
        {
          firstName: "Tushar",
          lastName: "Hirpara",
          email: "tushahirpara11@gmail.com",
          role: 1,
          password:
            "$2b$10$YKx51FMzCyMnfb3MkUJ43uLx94tELOGi/oMCOiCPl02Dcek2x/Xsy",
          isActive: true,
          contact: "7874546222",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: new Date()
        },
        {
          firstName: "Ravi",
          lastName: "Ramani",
          email: "ravi2906@gmail.com",
          role: 2,
          password: "1234",
          isActive: true,
          contact: "9925696038",
          createdAt: new Date(),
          updatedAt: new Date(),
          deletedAt: new Date()
        }
      ],
      {}
    ),

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Admins", null, {});
  }
};