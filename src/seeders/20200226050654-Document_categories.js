"use strict";

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "Document_categories",
      [
        {
          employeeCategory: 1,
          isActive: 1,
          name: "Aadhar",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          employeeCategory: 1,
          isActive: 1,
          name: "Voter Id",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          employeeCategory: 1,
          isActive: 1,
          name: "PAN Card",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          employeeCategory: 1,
          isActive: 1,
          name: "Passport",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          employeeCategory: 1,
          isActive: 1,
          name: "Photo",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          employeeCategory: 1,
          isActive: 1,
          name: "Degree",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          employeeCategory: 1,
          isActive: 1,
          name: "Mobile",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          employeeCategory: 1,
          isActive: 1,
          name: "Email",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          employeeCategory: 2,
          isActive: 1,
          name: "Experience",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          employeeCategory: 2,
          isActive: 1,
          name: "Salary Slip",
          createdBy: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    ),

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Document_categories", null, {});
  }
};