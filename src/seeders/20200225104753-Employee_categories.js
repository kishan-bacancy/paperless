"use strict";

module.exports = {
  up: queryInterface =>
    queryInterface.bulkInsert(
      "Employee_categories",
      [
        {
          createdBy: 1,
          category: "Fresher",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          createdBy: 1,
          category: "Experience",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    ),

  down: queryInterface => {
    return queryInterface.bulkDelete("Employee_categories", null, {});
  }
};