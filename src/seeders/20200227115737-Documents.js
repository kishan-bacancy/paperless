"use strict";

module.exports = {
  up: (queryInterface) => queryInterface.bulkInsert(
    'Documents',
    [
      {
        Employee_category: 1,
        Document_category_id: 1,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Employee_category: 1,
        Document_category_id: 2,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Employee_category: 1,
        Document_category_id: 3,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Employee_category: 1,
        Document_category_id: 4,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Employee_category: 1,
        Document_category_id: 5,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Employee_category: 1,
        Document_category_id: 6,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Employee_category: 2,
        Document_category_id: 7,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Employee_category: 2,
        Document_category_id: 8,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Employee_category: 1,
        Document_category_id: 9,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        Employee_category: 1,
        Document_category_id: 10,
        Document_path: "asd",
        Employee_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ],
    {}
  ),
  down: (queryInterface) => {
    return queryInterface.bulkDelete('Documents', null, {});
  }
};