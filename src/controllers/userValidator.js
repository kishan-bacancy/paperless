const Joi = require('joi');

export const registerAdminValidate = {
  body: {
    firstName: Joi.string().required().max(15),
    lastName: Joi.string().required().max(15),
    email: Joi.string().email().required().max(30),    
    password: Joi.string().required(),
    contact: Joi.string().required().max(10),
  },
};