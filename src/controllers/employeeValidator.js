const Joi = require('joi');

export const editEmployeeValidate = {
  body: {
    email: Joi.string().email().required(),
    mobile_no: Joi.string().required(),
    epicno: Joi.string().required(),
    name: Joi.string().required(),
    address: Joi.string().required(),
    dob: Joi.date().required(),
    gender: Joi.string().required(),
    aadhar_no: Joi.string().required(),
    pan_no: Joi.string().required(),
    passport_no: Joi.string(),
    passport_fileno: Joi.string(),
    issue_date: Joi.date(),
    expire_date: Joi.date(),
    enrollment_no: Joi.string().required(),
    college_name: Joi.string().required(),
    university_name: Joi.string().required(),
    college_place: Joi.string().required(),
    grade: Joi.string().required()
  }
};