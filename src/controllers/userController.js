import express from "express";
import bycrpt from "bcrypt";
import { Op } from "sequelize";
const path = require("path");

import { Admins } from "../models";

const app = express();
app.set("views", path.join(__dirname, "views"));

exports.addAdmin = async function (req, res) {
  let findAdmin;
  try {
    findAdmin = await Admins.findOne({
      where: {
        [Op.or]: [{ contact: req.body.contact }, { email: req.body.email }]
      }
    });
  } catch (err) {
    res.status(500).send(err);
  }

  if (findAdmin != null) {
    req.flash("error", "Admin already exists");
    res.redirect("/api/admin/subAdmin");
  } else {
    let createAdmin;
    try {
      req.body.password = await bycrpt.hash(req.body.password, 10);
      createAdmin = await Admins.create(req.body);
    } catch (err) {
      req.flash("error", "Something Went Wrong");
      res.redirect("/api/admin/subAdmin");
    }

    if (createAdmin) {
      req.flash("success", "Admin Created Successfully");
      return res.redirect("/api/admin/subAdmin");
    }

    req.flash("Error", "Admin isn't Created");
    res.redirect("/api/admin/subAdmin");
  }
};

exports.getAdmins = async function (req, res) {
  if (req.isAuthenticated && req.user.role == 1) {
    let allAdmins;
    try {
      allAdmins = await Admins.findAll({ where: { role: 2 } });
    } catch (err) {
      res.send("Something Went Wrong");
    }

    if (allAdmins) {
      return res.render("admin/viewAdmin", {
        data: allAdmins,
        session: req.user,
        successMsg: req.flash("success")
      });
    }
    req.flash("error", "Admin Not Found");
    res.render("admin/viewAdmin", {
      data: allAdmins,
      session: req.user,
      successMsg: req.flash("error")
    });
  } else {
    res.redirect("/api/admin/login");
  }
};

exports.changeStatus = async function (req, res) {
  let changeStatus;
  try {
    changeStatus = await Admins.update(
      { isActive: req.params.status },
      {
        where: { id: req.params.id }
      }
    );
  } catch (err) {
    req.flash("error", "Something Went Wrong");
    res.redirect("/api/admin/admins");
  }

  if (changeStatus) {
    req.flash("success", "Status Updated");
    return res.redirect("/api/admin/admins");
  }

  req.flash("error", "Status isn't Updated");
  res.redirect("/api/admin/admins");
};

exports.removeAdmin = async function (req, res) {
  let deleteAdmin;
  try {
    deleteAdmin = await Admins.destroy({ where: { id: req.params.id } });
  }
  catch (err) {
    req.flash("success", "Error in deleting admin");
    res.redirect("/api/admin/admins");
  }
  if (deleteAdmin) {
    req.flash("success", "Admin Deleted Successfully");
    return res.redirect("/api/admin/admins");
  }

  req.flash("success", "Admin isn't deleted.");
  res.redirect("/api/admin/admins");
};

exports.admin = (req, res) => {
  if (req.isAuthenticated() && req.user.role == 1) {
    res.render("createAdmin", {
      successMsg: req.flash("success"),
      session: req.user,
      errorMsg: req.flash("error")
    });
  } else {
    res.redirect("/api/admin/login");
  }
};

exports.login = (req, res) => {
  if (!req.isAuthenticated()) {
    req.flash("Error", "User Does not exist");
    res.render("login", { session: "", error: req.flash('Error') });
  } else {
    res.redirect("/api/admin/dashboard");

  }
};

exports.logout = (req, res) => {
  req.logout();
  res.json({
    error: false,
    redirect: "/login"
  });
};

exports.dashboard = (req, res) => {
  res.render("dashboard", { session: req.user });
};

exports.getAddEmpCategory = (req, res) => {
  res.render("addEmployeeCategory", { session: req.user, successMsg: '', errorMsg: '' });
};

exports.getAddDocumentCategory = (req, res) => {
  res.render("addDocumentCategory", { session: req.user, successMsg: '', errorMsg: '' });
};