import vision from '@google-cloud/vision';
import { verifyDocuments, isFileExists } from '../helper/extract';
import { calculateLimitAndOffset, paginate } from 'paginate-info';
import { Sequelize } from 'sequelize';
const moment = require('moment');
const employeeModel = require('../models').Employees;
const Op = Sequelize.Op;
const db = require("../models").Document_categories;
const documents = require("../models").Documents;

exports.addEmployee = async function (req, res) {
  let findEmployee;
  try {
    findEmployee = await employeeModel.findOne({
      where: {
        aadhar: { aadhar_no: req.body.aadhar_no }
      }
    });
  } catch (err) {
    res.render('employee/previewEmployee', {
      obj: obj, error: "Error in finding employee",
      session: req.user
    })
  }
  if (findEmployee != null) {
    req.flash("error", "Employee already exists");
    res.redirect("/employees");
  } else {
    const path = JSON.parse(req.body.path);
    let catId = JSON.parse(req.body.catid);
    delete catId.Mobile;
    delete catId.Email;

    let objPassport = {}, objSalary = {}, objExperience = {};
    if (req.body.passport_no || req.body.passport_fileno) {
      objPassport = {
        passport_no: req.body.passport_no,
        passport_fileno: req.body.passport_fileno,
        issue_date: moment(req.body.issue_date).format('YYYY-MM-DD'),
        expire_date: moment(req.body.expire_date).format('YYYY-MM-DD'),
        doc_path: {
          front: path['Passport'][0].path,
          back: path['Passport'][1].path
        }
      }
    } else {
      objPassport = null;
      delete catId.passport;
      delete catId['passport-back'];
    }
    let empCat;
    if (req.body.salary) {
      objSalary = {
        salary: req.body.salary,
        doc_path: {
          slip1: path['Salary Slip'][0].path,
          slip2: path['Salary Slip'][1].path,
          slip3: path['Salary Slip'][2].path
        }
      }
      objExperience = {
        company_name: req.body.company_name,
        joining_date: moment(req.body.joining_date).format('YYYY-MM-DD'),
        resign_date: moment(req.body.resign_date).format('YYYY-MM-DD'),
        designation: req.body.designation,
        company_email: req.body.company_email,
        doc_path: path['Experience'][0].path
      }
      empCat = 2;
    } else {
      objSalary = null;
      objExperience = null;
      empCat = 1;
    }
    let employeeCreate;
    try {
      employeeCreate = await employeeModel.create({
        createdBy: req.user.id,
        Employee_category: empCat,
        email: req.body.email,
        mobile_no: req.body.mobile_no,
        voterId: {
          epicno: req.body.epicno,
          name: req.body.name,
          address: req.body.address,
          dob: moment(req.body.dob).format('YYYY-MM-DD'),
          gender: req.body.gender,
          doc_path: {
            front: path['Voter Id'][0].path,
            back: path['Voter Id'][1].path
          }
        },
        aadhar: {
          aadhar_no: req.body.aadhar_no,
          doc_path: path['Aadhar'][0].path
        },
        pancard: {
          pan_no: req.body.pan_no,
          doc_path: path['PAN Card'][0].path
        },
        passport: objPassport,
        degree: {
          enrollment_no: req.body.enrollment_no,
          college_name: req.body.college_name,
          university_name: req.body.university_name,
          college_place: req.body.college_place,
          grade: req.body.grade,
          doc_path: path['Degree'][0].path
        },
        salarySlip: objSalary,
        experience: objExperience,
        isActive: true,
        modifiedBy: req.user.id,
        photo: path['Photo'][0].path,
      })
    } catch (err) {
      req.flash("error", "Employee isn't Created");
      res.redirect("/api/admin/dashboard");
    }

    if (employeeCreate) {
      const documentsPath = [], documentList = [], catIdList = [];
      for (let i in path) {
        for (let j in path[i]) {
          documentsPath.push(path[i][j].path);
        }
      }
      for (let j in catId) {
        catIdList.push(parseInt(catId[j]));
      }
      for (let i = 0; i < documentsPath.length; i++) {
        documentList.push({
          Employee_category: employeeCreate.dataValues.Employee_category,
          Document_category_id: catIdList[i],
          Document_path: documentsPath[i],
          Employee_id: employeeCreate.dataValues.id
        })
      }
      let documentCreated;
      try {
        documentCreated = await documents.bulkCreate(documentList);
      } catch (e) {
        req.flash("error", "Error in document uploading");
        res.redirect("/api/admin/dashboard");
      }
      if (documentCreated) {
        req.flash("success", "Employee Created Successfully");
      }
      res.redirect("/employees");
    } else {
      req.flash("error", "Employee isn't Created");
      res.redirect("/api/admin/addEmployee");
    }
  }
};

exports.freshersForm = async function (req, res) {
  try {
    const data = await db.findAll({
      where: {
        employeeCategory: 1
      }
    });
    res.render("freshersForm", { data: data, session: req.user });
  } catch (err) {
    res.send(err);
  }
};

exports.experienceForm = async function (req, res) {
  let data;
  try {
    data = await db.findAll({});
  } catch (e) {
    res.send(e);
  }
  res.render("experienceForm", { data: data, session: req.user });
};

exports.displayEmployees = async (req, res) => {
  try {
    const currentPage = req.query.page || 1;
    const pageSize = 10;
    const { limit, offset } = calculateLimitAndOffset(currentPage, pageSize);
    const { rows, count } = await employeeModel.findAndCountAll({ limit, offset });
    const meta = paginate(currentPage, count, rows, pageSize);
    res.render("employee/display", {
      emp: rows,
      meta: meta,
      success: req.flash("success"),
      error: req.flash("error"),
      session: req.user
    });
  } catch (err) {
    req.flash('success', 'Error in showing employees');
    res.redirect('/employee/display');
  }
};

exports.searchEmployee = async (req, res) => {
  try {
    let value = '%' + req.params.value + '%';
    const currentPage = 1;
    const pageSize = 10;
    const { limit, offset } = calculateLimitAndOffset(currentPage, pageSize);
    const { rows, count } = await employeeModel.findAndCountAll({
      where: {
        [Op.or]: [
          {
            email: { [Op.iLike]: value }
          },
          {
            voterId: {
              [Op.or]: [
                { name: { [Op.iLike]: value } },
                { gender: { [Op.iLike]: value } },
                { address: { [Op.iLike]: value } }
              ]
            }
          }
        ]
      }, limit, offset
    });
    const meta = paginate(currentPage, count, rows, pageSize);
    const obj = {
      meta: meta,
      emp: rows
    }
    res.json(obj);
  } catch (err) {
    res.send(err);
  }
};

exports.getEmployee = async (req, res) => {
  try {
    const emp = await employeeModel.findOne({
      where: {
        id: req.params.id
      }
    });
    if (emp) {
      res.render("employee/editEmployee", {
        emp: emp,
        success: req.flash("success"),
        error: req.flash("error"),
        session: req.user
      });
    } else {
      req.flash("error", "Employee not found.");
      res.redirect("/employees");
    }
  } catch (err) {
    req.flash('error', 'Invalid Id.');
    res.redirect('/employees');
  }
};

exports.removeEmployee = async (req, res) => {
  try {
    await employeeModel.destroy({
      where: {
        id: req.params.id
      }
    });
    req.flash("success", "Employee removed successfully.");
    res.redirect("/employees");
  } catch (err) {
    req.flash("error", "Error in deleting employee");
    res.redirect(`/employees`);
  }
};

exports.updateEmployeeStatus = async (req, res) => {
  let employeeActive;
  try {
    employeeActive = await employeeModel.update(
      {
        isActive: req.body.Active,
        modifiedBy: req.user.id
      },
      {
        where: {
          id: req.params.id
        }
      })
  } catch (err) {
    req.flash("error", "Error in activating or inactivating employee");
    res.redirect(`/employee/${req.params.id}`);
  }
  if (employeeActive) {
    let obj = {
      msg: 'Employeed updated'
    }
    res.json(obj);
  }
}

exports.updateEmployee = async (req, res) => {
  let employeeUpdate;
  try {
    employeeUpdate = await employeeModel.update(
      {
        modifiedBy: req.user.id,
        email: req.body.email,
        mobile_no: req.body.mobile_no,
        voterId: {
          epicno: req.body.epicno,
          name: req.body.name,
          address: req.body.address,
          dob: moment(req.body.dob).format('YYYY-MM-DD'),
          gender: req.body.gender
        },
        aadhar: {
          aadhar_no: req.body.aadhar_no
        },
        pancard: {
          pan_no: req.body.pan_no
        },
        passport: {
          passport_no: req.body.passport_no,
          passport_fileno: req.body.passport_fileno,
          issue_date: moment(req.body.issue_date).format('YYYY-MM-DD'),
          expire_date: moment(req.body.expire_date).format('YYYY-MM-DD')
        },
        degree: {
          enrollment_no: req.body.enrollment_no,
          college_name: req.body.college_name,
          university_name: req.body.university_name,
          college_place: req.body.college_place,
          grade: req.body.grade
        },
        salarySlip: {
          salary: req.body.salary
        },
        experience: {
          company_name: req.body.company_name,
          joining_date: moment(req.body.joining_date).format('YYYY-MM-DD'),
          resign_date: moment(req.body.resign_date).format('YYYY-MM-DD'),
          designation: req.body.designation,
          company_email: req.body.company_email
        }
      },
      {
        where: {
          id: req.params.id
        }
      }
    );
  } catch (err) {
    req.flash("error", "Error in updating employee");
    res.redirect(`/employee/${req.params.id}`);
  }
  if (employeeUpdate) {
    req.flash("success", "Employee updated successfully.");
    res.redirect(`/employee/${req.params.id}`);
  }
};

exports.extraction = async function (req, res) {
  const client = new vision.ImageAnnotatorClient({
    keyFilename: process.env.LICENSE_KEY
  });
  const obj = {};
  if (isFileExists(req.files['Aadhar'][0].path) === true) {
    try {
      const aadhar = await client.documentTextDetection(
        req.files['Aadhar'][0].path
      );
      verifyDocuments("aadhar", aadhar, obj);
    } catch (err) {
      throw new Error(err);
    }
  } else {
    res.send("Aadhar id not found");
    return false;
  }

  if (isFileExists(req.files["Voter Id"][0].path) === true) {
    try {
      const voter = await client.documentTextDetection(
        req.files["Voter Id"][0].path
      );
      verifyDocuments("voter", voter, obj);
    } catch {
      throw new Error(err);
    }
  } else {
    res.send("Voter id not found!");
    return false;
  }

  if (isFileExists(req.files["Voter Id"][1].path) === true) {
    try {
      const voter_back = await client.documentTextDetection(
        req.files["Voter Id"][1].path
      );
      verifyDocuments("voter-back", voter_back, obj);
    } catch (err) {
      throw new Error(err);
    }
  } else {
    res.send("Voter id back side can not found!");
    return false;
  }

  if (isFileExists(req.files["PAN Card"][0].path) === true) {
    try {
      const pan = await client.documentTextDetection(
        req.files["PAN Card"][0].path
      );
      verifyDocuments("pan", pan, obj);
    } catch (err) {
      throw new Error(err);
    }
  } else {
    res.send("Pan card not found!");
    return false;
  }
  if (req.files["Passport"]) {
    if (isFileExists(req.files["Passport"][0].path) === true) {
      try {
        const passport = await client.documentTextDetection(
          req.files["Passport"][0].path
        );
        verifyDocuments("passport", passport, obj);
      } catch (err) {
        throw new Error(err);
      }
    } else {
      obj.passport = "";
    }

    if (isFileExists(req.files["Passport"][1].path) === true) {
      try {
        const passport_back = await client.documentTextDetection(
          req.files["Passport"][1].path
        );
        verifyDocuments("passport-back", passport_back, obj);
      } catch (err) {
        throw new Error(err);
      }
    } else {
      obj.passport_back = "";
    }
  }

  if (isFileExists(req.files["Degree"][0].path) === true) {
    try {
      const degree = await client.documentTextDetection(
        req.files["Degree"][0].path
      );
      verifyDocuments("degree", degree, obj);
    } catch (err) {
      throw new Error(err);
    }
  } else {
    res.send("Degree not found!");
    return false;
  }

  if (req.files["Salary Slip"]) {
    if (isFileExists(req.files["Experience"][0].path) === true) {
      try {
        const experience = await client.documentTextDetection(
          req.files["Experience"][0].path
        );
        verifyDocuments("experience", experience, obj);
      } catch (err) {
        throw new Error(err);
      }
    } else {
      obj.experience = "";
    }
    if (isFileExists(req.files["Salary Slip"][2].path) === true) {
      try {
        const salary = await client.documentTextDetection(
          req.files["Salary Slip"][2].path
        );
        verifyDocuments("salary", salary, obj);
      } catch (err) {
        throw new Error(err);
      }
    } else {
      obj.salary = "";
    }
  }
  obj.photo = req.files["Photo"][0].path;
  obj.email = req.body.Email;
  obj.mobile = req.body.Mobile;

  res.render('employee/previewEmployee', {
    obj: obj,
    success: "",
    session: req.user,
    path: JSON.stringify(req.files),
    catid: JSON.stringify(req.body)
  });
};