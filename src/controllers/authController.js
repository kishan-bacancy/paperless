import bycrpt from "bcrypt";
import passport from "passport";
const LocalStrategy = require("passport-local").Strategy;
const { Admins } = require("./../models");

export const local = passport.use(
  new LocalStrategy(function(username, password, done) {
    Admins.findOne({
      where: {
        email: username
      }
    }).then(function(user, err) {
      if (err) {
        throw new Error(err);
      }
      if (user) {
        if (bycrpt.compareSync(password, user.password)) {
          return done(null, user);
        } else {
          return done(null, false);
        }
      } else {
        return done(null, false);
      }
    });
  })
);

export const serializaUser = passport.serializeUser(function(user, done) {
  done(null, user);
});

export const deserializeUser = passport.deserializeUser(function(user, done) {
  done(null, user);
});