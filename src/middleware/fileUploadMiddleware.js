import { upload } from "../../src/upload/uplods";

function validateFiles(err, req, res, next) {
  upload.fields([
    { name: "Aadhar", maxCount: 1 },
    { name: "PAN Card", maxCount: 1 },
    { name: "Passport", maxCount: 2 },
    { name: "Voter Id", maxCount: 2 },
    { name: "Degree", maxCount: 10 },
    { name: "Photo", maxCount: 1 }
  ])
  next();
}

module.exports = { validateFiles }