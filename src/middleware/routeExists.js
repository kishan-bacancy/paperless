exports.validateRoutes = (req, res, next) => {
  if(res.status() !== 400) {
    res.redirect('/hello')
  } else {
    next();
  }
}