import fs from 'fs';
import moment from 'moment';

/* Fetch data from image */
function getTextBlocks(visionResults) {
  let textBlocks = [];
  let blockIndex = 0;
  visionResults.forEach(result => {
    result.fullTextAnnotation.pages.forEach(page => {
      textBlocks = textBlocks.concat(page.blocks.map(block => {
        return {
          blockIndex: blockIndex++,
          text: fetchData(block)
        }
      }));
    });
  });
  return textBlocks;
}

function fetchData(block) {
  let result = '';
  block.paragraphs.forEach(paragraph => {
    paragraph.words.forEach(word => {
      word.symbols.forEach(symbol => {
        result += symbol.text;
        if (symbol.property && symbol.property.detectedBreak) {
          const breakType = symbol.property.detectedBreak.type;
          if (['EOL_SURE_SPACE', 'SPACE'].includes(breakType)) {
            result += " ";
          }
          if (['EOL_SURE_SPACE', 'LINE_BREAK'].includes(breakType)) {
            result += "\n";
          }
        }
      })
    })
  })
  return result;
}

/* To split data */
function splitArray(data) {
  const text = getTextBlocks(data);
  const splittedArray = text.map((ele) => {
    return ele.text.split('\n');
  });
  return splittedArray;
}

/* Converting date */
function convertDate(inputDate, format) {
  let dateObject;
  switch (format) {
    case 'DD/MM/YYYY':
      dateObject = moment(inputDate, format).toDate();
      break;

    case 'DD-Mon-YY':
      dateObject = moment(inputDate, format).toDate();
      break;
  }
  const date = moment(dateObject).format('YYYY-MM-DD');
  return date;
}

/* Verify all document using regex */
exports.verifyDocuments = (document, data, obj) => {
  switch (document) {
    case 'aadhar':
      splitArray(data).flat().forEach((word) => {
        if (word.trim().match(/^\d{4}\s\d{4}\s\d{4}.*$/)) {
          obj.aadhar = word.slice(0, 14).split(" ").join("");
        }
      });
      break;

    case 'voter':
      splitArray(data).flat().forEach((word) => {
        if (word.trim().match(/([A-Za-z]){3}([0-9]){7}/)) {
          obj.voterId = word.trim().match(/([A-Za-z]){3}([0-9]){7}/)[0];
        }
        if (word.trim().match(/^Name : +[A-Za-z]+ [A-Za-z].*/)) {
          obj.name = word.slice(7, (word.length));
        }
      });
      break;

    case 'voter-back':
      splitArray(data).flat().forEach((word) => {
        if (word.trim().match(/Female|male|female|Male/)) {
          obj.gender = word.trim().match(/Female|male|female|Male/)[0].trim();
        }
        const dateRegex = /: [0-9]{2}\/[0-9]{2}\/[0-9]{4}/;
        if (word.trim().match(dateRegex)) {
          obj.dob = convertDate(word.trim().match(dateRegex)[0].slice(2, 12), 'DD/MM/YYYY');
        }
      });
      splitArray(data).forEach((word) => {
        word.forEach(ele => {
          if (ele.match(/^Address : /)) {
            word.toString().slice(0, 10);
            obj.address = word.toString();
          }
        });
      });
      break;

    case 'pan':
      splitArray(data).flat().forEach((word) => {
        if (word.trim().match(/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/)) {
          obj.panNo = word;
        }
      });
      break;

    case 'passport':
      const passportDate = [], passportNo = /^[a-zA-Z]{1}\d{7}$/,
        date = /[0-9]{2}\/[0-9]{2}\/[0-9]{4}/;
      splitArray(data).flat().forEach((word) => {
        if (word.trim().match(passportNo)) {
          obj.passportNo = word;
        }
        if (word.trim().match(date)) {
          passportDate.push(word.trim().match(date)[0]);
        }
      });
      passportDate.splice(2)

      obj.issueDate = convertDate(passportDate[0], 'DD/MM/YYYY');
      obj.expiryDate = convertDate(passportDate[1], 'DD/MM/YYYY');
      break;

    case 'passport-back':
      splitArray(data).flat().forEach((word) => {
        if (word.match(/^[A-Z0-9]{12,15}$/)) {
          obj.fileNo = word;
        }
      });
      break;

    case 'degree':
      obj.collegeName = splitArray(data)[1][1].slice(3, 41);
      obj.degreeClass = splitArray(data)[1][5];
      break;

    case 'experience':
      obj.companyName = splitArray(data)[0][0].trim();
      obj.doj = convertDate(splitArray(data)[9][0].slice(1, 11),'DD-Mon-YY');
      obj.resignationDate = convertDate(splitArray(data)[11][0],'DD-Mon-YY');
      obj.designation = splitArray(data)[13][0].slice(1, splitArray(data)[13][0].length);
      break;

    case 'salary':
      obj.salary = splitArray(data)[8][0];
      break;
  }
}

exports.isFileExists = (path) => {
  try {
    if (fs.existsSync(path)) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    throw new Error(err);
  }
}