# Paperless

Paperless is used to fetch the details from the documents using google's cloud vision API and stores that data in the database. It is written in NodeJs and uses sequelizer as ORM and Postgres as database. 

### Prerequisites
```bash 
node [v8.10.0](https://nodejs.org/en/)
postgres [12.1](https://www.postgresql.org/download/)
```

### Installation
You can download this repo or clone this using the below command
```bash
$ git clone https://bitbucket.org/kishan-bacancy/paperless.git
```

Install all dependencies and modules:
$ cd paperless
```bash
$ npm i
$ run at : 127.0.0.1:3000
```

### Setup
Once you clone or download project go into you folder
```bash
create a .env file
now copy ***.env.local*** file to ***.env***
npm install (it will install all the dependencies required for the project)
```  
 
### .env file

```bash 
DB_HOST=localhost
DB_USER=Abhishek
DB_PASS=1234
DB_NAME=login
DB_DIALECT=postgres
DB_PORT=5432 
APP_HOST=localhost
APP_PORT=3000
SECRET=adasxovnklnqklnkjdsankdnw
LICENSE_KEY=k@asDF@.h3d@
```

### Tech

Paperless uses mostly this technologies.

* [ Express.js ] - Awesome web-based text editor
* [ Passport.js] - Simple, unobtrusive authentication for Node.js.
* [ EJS ] - Markdown parser done right. Fast and easy to extend.
* [ Bootstrap ] - Great UI for modern web apps
* [ Node.js ] - Evented I/O for the backend
* [ Sequelize ] - A promise based NodeJs ORM for Postgres
* [ Postgres ] - Relational open source database 